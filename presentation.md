---
author: "Stefan Schweter"
title: "dlib - A toolkit for making real world machine learning and data analysis applications in C++"
institute: "Masterseminar: Klassifikation und Clustering, Wintersemester 2016/2017, Dozent: Stefan Langer"
date: "19.12.2016"
navigation: horizontal
...

# Überblick

---

- Einführung
- Bus-Faktor
- Machine Learning Algorithmen
- MITIE
- Demo: Hidden Markov SVM für Namenserkennung
- Demo: Deep Learning I (Objekterkennung)
- Ausblick Seminararbeit


# dlib

---

![dlib Logo](dlib-logo.png)\


- C++ Bibliothek für maschinelles Lernen
- Hauptautor: Davis King
- Entwicklung seit 2002
- Intuitive API - Namespace `dlib::`
- Getestet auf MS Windows, Linux, Mac OS X

---

- Aktuelle Version *19.2*
- Minorrelease alle 2 Monate
- *Boost Software License* (*GPL* kompatibel^[Siehe \url{https://www.gnu.org/licenses/license-list.de.html\#boost}])
- Unterstützung von *Office of the Director of National Intelligence* (ODNI) und
  *Advanced Research Projects Activity* (IARPA) in 2014

---

- Sehr gute Dokumentation und Beispielprogramme
- Sehr umfangreiche Testsuite

![Code quality](code_quality.png)\


# Bus-Faktor

---

- Der Bus-Faktor (oder auch Truck Number) bezeichnet die Zahl an Projektmitarbeitern,
  die ausfallen können, ohne das Projekt zu gefährden


![Avelino et. al. Paper](avelino_paper.png)

---

- Faktor 1: Sehr schlecht

- 46 Prozent von 133 populären Projekte auf GitHub haben einen Bus-Faktor von 1^[Siehe \url{https://heise.de/-2752849}]

---

- Bus-Faktor von *machine learning* Bibliotheken^[Berechnung siehe \url{https://github.com/aserg-ufmg/Truck-Factor}]:

    | Projekt        | Bus-Faktor | Beteiligte
    | -------------- | ---------- | --------------
    | `Scikit-learn` | 10         |  757
    | `Theano`       |  9         |  270
    | `caffe`        |  4         |  219
    | `NLTK`         |  2         |  185
    | `TensorFlow`   |  2         |  552
    | `Keras`        |  1         |  313
    | `Torch`        |  1         |  109
    | `dlib`         |  1         |   47

# Machine Learning Algorithmen

---

- Klassifikation:
    - Binäre und Multiklassen Klassifikation mittels Support-Vektor-Maschine (mit diversen Kernel)

- Clustering
    - *k-means*
    - Newman Cluster
    - Chinese whispers

- Regression
    - *kernel recursive least squares* (KRLS)
    - Support vector regression

- Deep Learning
    - LeNet (convolutional neural network)

---

- Algorithmen aus mehr als 30 wissenschaftlichen Veröffentlichungen implementiert

- Mehr im *Machine learning guide*^[Siehe \url{http://dlib.net/ml_guide.svg}] zu finden

# MITIE

---

- Programm zur Informationsextraktion (*Named Entity Recognition* und *Relation extraction*)
- Entwickelt am *Massachusetts Institute of Technology* (MIT)
- Verwendet *dlib* zur Klassifikation
- Trainierte Modelle für Englisch, Spanisch und Deutsch^[Siehe \url{https://github.com/mit-nlp/MITIE/commit/f124510782b2054e264f288c65404763fae32b3f}]
- API für C/C++, Java, MATLAB, Java, Python und R
- Vergleichbare Ergebnisse^[Siehe \url{https://github.com/mit-nlp/MITIE/wiki/Evaluation}] für CoNLL 2003 NER *shared task* zu Systemen wie
  *Stanford NER*


# Demo: Hidden Markov SVM für Namenserkennung

---

- Beispiel für Namenserkennung mit Hidden Markov Support Vektor Maschinen^[Siehe \url{http://www.aaai.org/Papers/ICML/2003/ICML03-004.pdf}]

![Altun et. al. Paper](hm-svm.png)\


# Demo: Deep Learning I (Objekterkennung)

---

- "Dog hipsterizer"^[Siehe \url{http://blog.dlib.net/2016/10/}]

![dlib Blog](barkhaus_hip.jpg)\


# Ausblick Seminararbeit

---

- Erweiterung des am *CIS* entwickelten Programms zur Satzendeerkennung (*EOS*)
- Bisher rein regelbasierte Klassifikation von potentiellen Satzgrenzen
- Klassifikator soll mittels Support-Vektor-Maschine auf einem großen Korpus
  für verschiedenen Sprachen trainiert werden
- Gelernter Klassifikator soll *EOS* bei der Bewertung von potentiellen
  Satzgrenzen unterstützen

# EOS - Programm zur Satzendeerkennung

---

## Geschichte

- *EOS* ("End of sentence") wurde seit dem Sommersemester 2009 von mehreren Studenten
  unter Leitung von Herrn Dr. Maximilian Hadersbeck weiterentwickelt:

- Mitwirkende (Sommersemester 2009): Susanne Peters, Michael Mandl, Daniel Bruder, David Kaumanns und Jonathan Cummings

- Mitwirkende (Sommersemester 2010): Florian Fink, Susanne Peters, Daniel Bruder, David Kaumanns, Dino Azzano, Estelle Perez und Simon Thum

- Mitwirkende (Wintersemester 2012/2013): Benno Weck, Jasmin Chebib, Martin Röhrs, Matthias Lindinger, Eamonn Lawlor, Angela Krey und Stefan Schweter

## Aufbau - Agenten (1)

- Audhumbla: Markiert potenzielle Satzgrenzen

- Abbreviation: Abkürzungserkennung via Abkürzungsliste, morphologische
  Abkürzungserkennung bei unbekannten Abkürzung

- Cross: Überprüft, ob das Wort nach dem potenziellen Satzende
  im Text nochmals kleingeschrieben, oder in einer eingebundenen Frequenzliste
  von häufig kleingeschriebenen Wörtern existiert. Trifft eines der beiden Fälle
  zu, so handelt es sich wahrscheinlich um ein Satzende

## Aufbau - Agenten (2)

- Minuscule: Regelbasierter Agent, der überprüft, wie es nach einem potenziellen
  Satzende im Text weitergeht (zwei Zeilenumbrüche: Satzende, Kleinbuchstabe:
  Kein Satzende etc.)

- Primus: Überprüft, ob das Wort nach einem potenziellen Satzende in einer Liste
  von häufigen Satzanfängen steht

- Regex: Agent kann anhand einer Liste von regulären Ausdrücken potenzielle
  Satzenden bewerten. Auszug:

    ```
    -/(str\.)$/i
    ```

## Scoreberechnung

- Jedes potenzielle Satzende wird von den genannten Agenten *bewertet* - jeder
  Agent liefert dafür einen Score zurück, der im Bereich von **-127** bis **+127** liegt

- **-127** bedeutet: Der Agent vermutet, dass *kein* Satzende vorliegt

- **+127** bedeutet: Der Agent vermutet, dass *ein* Satzende vorliegt

- Nach dem alle Agenten die potenzielle Satzgrenze bewertet haben, werden alle
  Bewertungen zusammengerechnet. Liegt der Wert über einen bestimmten
  *Schwellenwert* so liegt ein Satzende vor.

## Demoseite

- *EOS* ist unter \url{http://eos.cis.lmu.de} erreichbar

## API

- Die *EOS* *API* kann z.B. per Kommandozeile angesprochen werden:

    ```bash
    curl -d '{"text":"Herr Prof. Dr. Müller geht heute
               in die Universität.", "language": "de"}' \
            api.eos.cis.lmu.de/api/v1
    ```

- Beispielrückgabe:

    ```json
    {
      "result" : "Herr Prof. Dr. Müller geht heute
                  in die Universität.</eos>\n"
    }
    ```

## &emsp;

- Fragen?

- Vielen Dank für die Aufmerksamkeit!

- Folien und Programme sind unter \url{https://gitlab.com/stefan-it/dlib-presentation} zu finden
