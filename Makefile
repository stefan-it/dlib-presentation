SILENT ?= @

BEAMER_PRESENTATION := $(patsubst %.md,%.pdf,$(wildcard *.md))

define PANDOC.OPTS
--to beamer \
--smart \
--slide-level=2 \
--variable header-includes='\renewcommand\thempfootnote{\arabic{mpfootnote}}' \
--variable header-includes='\newcommand{\shortauthor}{Stefan Schweter}' \
--variable header-includes='\newcommand{\shorttitle}{dlib - A toolkit for making real world machine learning in C++}' \
--include-in-header=presentation-footer.tex \
--variable theme:Warsaw \
--variable lang=de-DE \
--latex-engine=xelatex
endef

%.pdf: %.md
	$(SILENT) pandoc $(PANDOC.OPTS) $< -o $@

.PHONY: all
all: $(BEAMER_PRESENTATION)
