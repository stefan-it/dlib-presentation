# dlib Präsentation

dlib Präsentation für das Masterseminar: *Klassifikation und Clustering* im
Wintersemester 2016/2017. Centrum für Informations- und Sprachverarbeitung (CIS),
Ludwig-Maximilians-Universität.

Die Kursseite ist [hier](http://www.cis.uni-muenchen.de/~stef/seminare/klassifikation_2016/)
zu erreichen.

# Beamer-Slides

Um eine PDF Präsentation zu erzeugen muss `pandoc` >= *1.16* installiert sein.
Danach kann die Präsentation durch:

```bash
make
```

erstellt werden.

# Demoprogramm

Das Demoprogramm im `demo_crf_svm` kann mit:

```bash
g++ -std=c++11 -Ofast main.cpp -ldlib
```

kompiliert werden.
