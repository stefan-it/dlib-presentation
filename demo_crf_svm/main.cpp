#include <cctype>
#include <dlib/string.h>
#include <dlib/svm_threaded.h>
#include <iostream>
#include <regex>

class feature_extractor {
public:
  using sequence_type = std::vector<std::string>;

  const static bool use_BIO_model = true;
  const static bool use_high_order_features = true;
  const static bool allow_negative_weights = true;
  unsigned long window_size() const { return 2; }

  unsigned long num_features() const { return 2; }

  template <typename feature_setter>
  void get_features(feature_setter &set_feature, const sequence_type &sentence,
                    unsigned long position) const {
    auto current_word_starts_upper = isupper(sentence[position][0]);

    auto prev_word_has_punct = current_word_starts_upper && position > 0 &&
                               !ispunct(sentence[position - 1][0]);

    set_feature(0, !current_word_starts_upper);
    set_feature(1, prev_word_has_punct);
  }
};

void serialize(const feature_extractor &, std::ostream &) {}
void deserialize(feature_extractor &, std::istream &) {}

void make_training_examples(
    std::vector<std::vector<std::string>> &samples,
    std::vector<std::vector<std::pair<unsigned long, unsigned long>>> &segments)

{
  std::vector<std::pair<unsigned long, unsigned long>> names;

  samples.push_back(dlib::split("Tritt ehrerbietig vor Margareten zurück ."));
  names.push_back(std::make_pair(3, 4));
  segments.push_back(names);
  names.clear();

  samples.push_back(dlib::split("Wollte nach Frau Marthe Schwerdlein fragen !"));
  names.push_back(std::make_pair(2, 5));
  segments.push_back(names);
  names.clear();

  samples.push_back(dlib::split("Er waͤr’ ein Narr ! Ein flinker Jung’ ."));
  names.push_back(std::make_pair(3, 4));
  names.push_back(std::make_pair(7, 8));
  segments.push_back(names);
  names.clear();

  samples.push_back(dlib::split("Das Kind erſtickt , die Mutter platzt ."));
  names.push_back(std::make_pair(2, 4));
  names.push_back(std::make_pair(5, 6));
  segments.push_back(names);
  names.clear();

  samples.push_back(dlib::split("Du gehſt nun fort ? O Heinrich koͤnnt’ ich mit !"));
  names.push_back(std::make_pair(6, 7));
  segments.push_back(names);
  names.clear();

  samples.push_back(dlib::split("Man tanzt , man ſchwazt , man kocht , man trinkt , man liebt ;"));
  segments.push_back(names);
  names.clear();
}

void print_segment(const std::vector<std::string> &sentence,
                   const std::pair<unsigned long, unsigned long> &segment) {
  for (auto i = segment.first; i < segment.second; ++i) {
    std::cout << sentence[i] << " ";
  }
  std::cout << "\n";
}

int main() {
  std::vector<std::vector<std::string>> samples;
  std::vector<std::vector<std::pair<unsigned long, unsigned long>>> segments;
  make_training_examples(samples, segments);

  dlib::structural_sequence_segmentation_trainer<feature_extractor> trainer;
  trainer.set_c(2);
  trainer.set_num_threads(4);

  dlib::sequence_segmenter<feature_extractor> segmenter =
      trainer.train(samples, segments);

  std::cout << "Erkannte Namen im Trainingsset:\n";
  for (auto i = 0; i < samples.size(); ++i) {
    auto segment = segmenter(samples[i]);
    for (unsigned long j = 0; j < segment.size(); ++j) {
      print_segment(samples[i], segment[j]);
    }
  }

  std::cout << "\nErkannte Namen im Testset:\n";
  auto sentence = dlib::split("Ihr naht euch wieder , ſchwankende Geſtalten !");
  auto segment = segmenter(sentence);
  for (auto j = 0; j < segment.size(); ++j) {
    print_segment(sentence, segment[j]);
  }

  std::cout << "\nprecision, recall, f1-score: "
            << dlib::test_sequence_segmenter(segmenter, samples, segments);
  std::cout << "precision, recall, f1-score: "
            << dlib::cross_validate_sequence_segmenter(trainer, samples, segments, 5);


  dlib::serialize("segmenter.dat") << segmenter;
  dlib::deserialize("segmenter.dat") >> segmenter;
}
